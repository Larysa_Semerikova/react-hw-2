import React, { Component } from "react";
import "../src/App.scss";
import ProductList from "./components/ProductList";
import Header from "./components/Header";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      cartItems: JSON.parse(localStorage.getItem("cartItems")) || [],
      favoriteItems: JSON.parse(localStorage.getItem("favoriteItems")) || [],
    };
  }

  getProducts() {
    fetch("products.json")
      .then((res) => res.json())
      .then((products) => {
        this.setState({
          products: products,
        });
      });
  }

  componentDidMount() {
    this.getProducts();
  }

  handleAddToCart = (id) => {
    let data = this.state.products.find((item) => item.id === id);
    this.setState(
      {
        cartItems: [...this.state.cartItems, data],
      },
      () => {
        localStorage.setItem("cartItems", JSON.stringify(this.state.cartItems));
      }
    );
  };

  handleAddToFavorites = (id) => {
    let data = this.state.products.find((item) => item.id === id);
    this.setState(
      {
        favoriteItems: [...this.state.favoriteItems, data],
      },
      () => {
        localStorage.setItem(
          "favoriteItems",
          JSON.stringify(this.state.favoriteItems)
        );
      }
    );
  };

  handleRemoveFromFav = (id) => {
    let data = this.state.favoriteItems.filter((item) => {
      return item.id !== id;
    });
    this.setState(
      {
        favoriteItems: data,
      },
      () => {
        localStorage.setItem(
          "favoriteItems",
          JSON.stringify(this.state.favoriteItems)
        );
      }
    );
  };

  render() {
    const { products, cartItems, favoriteItems } = this.state;
    return (
      <>
        <Header cartItems={cartItems} favoriteItems={favoriteItems} />
        <ProductList
          products={products}
          handleAddToFavorites={this.handleAddToFavorites}
          handleRemoveFromFav={this.handleRemoveFromFav}
          handleAddToCart={this.handleAddToCart}
        />
      </>
    );
  }
}

export default App;
