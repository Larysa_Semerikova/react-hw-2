import React from "react";
import { Component } from "react";
import "../components/ButtonComponent.scss";
import PropTypes from "prop-types";


class Button extends Component {
  render() {
    const { backgroundColor, text, onClick } = this.props;

    return (
      <button className="btn" style={{ backgroundColor }} onClick={onClick}>
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func
}

export default Button;
