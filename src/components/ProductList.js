import React, { Component } from "react";
import ProductCard from "./ProductCard";
import PropTypes from "prop-types";

class ProductList extends Component {
  render() {
    const {
      products,
      handleAddToCart,
      handleAddToFavorites,
      handleRemoveFromFav,
    } = this.props;

    return (
      <div className="product-list">
        {products.map((product) => (
          <ProductCard
            key={product.id}
            product={product}
            handleAddToFavorites={handleAddToFavorites}
            handleRemoveFromFav={handleRemoveFromFav}
            handleAddToCart={handleAddToCart}
          />
        ))}
      </div>
    );
  }
}

ProductList.propTypes = {
  products: PropTypes.array.isRequired,
  handleAddToFavorites: PropTypes.func,
  handleRemoveFromFav: PropTypes.func,
  handleAddToCart: PropTypes.func,
};

export default ProductList;
