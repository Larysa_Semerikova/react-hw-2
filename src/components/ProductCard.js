import React, { Component } from "react";
import "./ProductCard.scss";
import Button from "./ButtonComponent";
import Modal from "./ModalComponent";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-regular-svg-icons";
import { faStar as farStar } from "@fortawesome/free-solid-svg-icons";

class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.product = props.product;
    this.handleAddToFavorites = props.handleAddToFavorites;
    this.handleRemoveFromFav = props.handleRemoveFromFav;
    this.handleAddToCart = props.handleAddToCart;
    this.state = {
      modalOpen: false,
      isFavorite:
        JSON.parse(
          localStorage.getItem(`isFavorite_${this.product.title}`)
        ) || false,
    };
  }

  handleFav = () => {
    const newFav = !this.state.isFavorite;
    if (newFav) {
      this.setState({ isFavorite: true });
      this.handleAddToFavorites(this.product.id);
    } else {
      this.setState({ isFavorite: false });
      this.handleRemoveFromFav(this.product.id);
    }
    localStorage.setItem(
      `isFavorite_${this.product.title}`,
      JSON.stringify(newFav)
    );
  };

  addToCart = () => {
    this.handleAddToCart(this.product.id);
    this.setState({ modalOpen: false });
  };

  isModalOpen = () => {
    this.setState({ modalOpen: true });
  };

  handleCloseModal = () => {
    this.setState({
      modalOpen: false,
    });
  };

  handleClickOutsideModal = (e) => {
    if (e.target.classList.contains("modal-container")) {
      this.handleCloseModal()
    }
  };

  render() {
    const { product } = this.props;
    const { modalOpen, isFavorite } = this.state;

    return (
      <div className="card">
        <div className="card-header">
          <img
            className="card-img"
            src={product.image}
            alt="image of product"
          />
          <FontAwesomeIcon
            className="card-fav_item"
            icon={isFavorite ? farStar : faStar}
            onClick={this.handleFav}
          />
        </div>
        <div className="card-body">
          <h3 className="card-title">{product.title}</h3>
          <p className="card-text">{product.color}</p>
          <div className="card-footer">
            <h4 className="card-text">{product.price} uah</h4>
            <div className="card-footer-icons">
              <Button text="To cart" onClick={this.isModalOpen} />
              {modalOpen && (
                <Modal
                  text="Add this item to the cart?"
                  closeButton={true}
                  closeModal={this.handleCloseModal}
                  actions={
                    <>
                      <button className="confirmBtn" onClick={this.addToCart}>
                        Add
                      </button>
                      <button
                        className="cancelBtn"
                        onClick={this.handleCloseModal}
                      >
                        Cancel
                      </button>
                    </>
                  }
                  clickOutside={this.handleClickOutsideModal}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    title: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.number
  }),
};

export default ProductCard;
