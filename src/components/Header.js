import React, { Component } from "react";
import "./Header.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faCartArrowDown } from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";

class Header extends Component {
  render() {
    const { favoriteItems, cartItems} = this.props;
    return (
      <div className="header">
        <div className="header-title">
          <h1 className="header-brand">Brand's Outlet</h1>
        </div>

        <div className="header-icons">
          <FontAwesomeIcon icon={faStar} />
          {
            <div className="favorite-total">
              {favoriteItems.length}
            </div>
          }
          <FontAwesomeIcon icon={faCartArrowDown} />
          {<div className="cart-total">{cartItems.length}</div>}
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  cartItems: PropTypes.array.isRequired,
  favoriteItems: PropTypes.array.isRequired,
}

export default Header;
